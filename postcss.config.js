module.exports = {
	plugins: {
		"postcss-preset-env": {
			stage: 0
		},
		"rucksack-css": {},
		"postcss-font-variant": {},
		"postcss-system-monospace": {},
		"postcss-light-text": {},
		"laggard": {},
		"autoprefixer": {}
	}
};
