> this repo is mostly unmaintained at this point. i haven't been using parcel for new projects for a while now, i've mostly shifted to using [eleventy](https://www.11ty.dev/) for purely static sites, and often [Next.js](https://nextjs.org/) for projects with more complex server-side rendering requirements. i haven't kept up with parcel 2.0 dev, i tried it for a couple projects a while ago and came up against some roadblocks then, and haven't gone back to see if they were fixed since.

# web starter template

uses [parcel](https://parceljs.org/) + some config

i recommend using [degit](https://www.npmjs.com/package/degit) to make a copy of this for yourself to start from:
```sh
npx degit gitlab:emma/web-template my-new-project
```
(if you run `npm install --global degit` you can then omit the `npx` bit)

## how to use

### development

for development, run `npm start`, and then open your browser to [localhost:1234](http://localhost:1234) after a moment.

everything should be hot-reloaded, so just save your files and the browser should update automatically.

### builds

run `npm run build` to make an optimized minimized build in the folder called `dist`. files there can then be uploaded to whatever web server you've got.

## other notes

- feel free to add other plugins if you want
- if you want files to be copied as-is into the built site, put them in a folder called `static`